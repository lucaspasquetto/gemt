/*

 $token  = "blitsoftwares";//filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
 $action = "city_search";//filter_input(INPUT_POST,'action',FILTER_SANITIZE_STRING);
 $state  = "mt";//filter_input(INPUT_POST,'state',FILTER_SANITIZE_STRING);
 $device = md5($token)==TOKEN ? true : false;

 */

var URL_WS = "http://www.guiaempresarialmt.com.br/ws/ws.php";
var NETWORK ;
var NETWORK_TYPE;
var db = null;
var tstCidade = false;

getCidadesWS = function(){

    var teste;
    $.ajax({
        type: 'POST',
        url: URL_WS,
        data: {
            token: "blitsoftwares",
            action: "city_search",
            id: "mt"
        },
        async:true,
        beforeSend: function() {
            showLoader()
        },
        success: function(data) {
            if(data!="Error") {
                data = jQuery.parseJSON(data);
                if(data.length>0) {
                    $(".ui-select-city option").remove();
                    $("#listaOffine li").remove();
                    $(".ui-select-city").append('<option class"itemCidade" value="" selected>Selecione uma cidade</option>');
                    popula(data,0);
                }

            }else{
                navigator.notification.alert("Falha na pesquisa, tente novamente.");
                hideLoader();
                teste = false;
            }
        },
        error: function(xhr) {
            navigator.notification.alert(xhr);
            hideLoader();
            teste = false;
        },
        dataType: "text"
    });
    return teste;
}

populaOffline = function(select,id,cidade,uf){
    var tmp = '';
    if(select) {
        //tmp = '<li><h2> <select class="ui-flipswitch" data-role="flipswitch" onchange="sincroniza(' + id + ')" name="flip-3" id="' + id + '"  > <option value="0" >Web</option> <option value="1"  selected>Local</option> </select> ' + cidade + '/' + uf + '</h2> </li>';
        tmp = '<li><h2> <input type="checkbox" id="' + id + '" class="flip" data-role="flipswitch" onchange="sincroniza(' + id + ')" checked> ' + cidade + '/' + uf + '</h2> </li>';
        $("#listaOffine").append(tmp);
        $( "#"+id ).flipswitch({
            onText: "On",
            offText: "Web"
        });
        $( "#"+id ).flipswitch('refresh');
    } else {
        //tmp = '<li><h2> <select  class="ui-flipswitch" data-role="flipswitch" onchange="sincroniza(' + id + ')" name="flip-3" id="' + id + '" > <option value="0" selected>Web</option> <option value="1" >Local</option> </select> ' + cidade + '/' + uf + '</h2> </li>';
        tmp = '<li><h2> <input type="checkbox" id="' + id + '" class="flip" data-role="flipswitch" onchange="sincroniza(' + id + ')"> ' + cidade + '/' + uf + '</h2> </li>';
        $("#listaOffine").append(tmp);
        $( "#"+id ).flipswitch({
            onText: "On",
            offText: "Web"
        });
        $( "#"+id ).flipswitch('refresh');
    }
    $('#listaOffine').listview('refresh').trigger('create');
}

populaOnline = function(id,cidade,uf){
    $(".ui-select-city").append(
        '<option class"itemCidade" value="' + id + '">' + cidade + '/' + uf + '</option>'
    );
}

popula = function (data, pos) {
    if(pos<data.length) {
        var id      = data[pos].id;
        var cidade  = data[pos].cidade;
        var uf      = data[pos].uf;
        populaOnline(id,cidade,uf);
        db.transaction(
            function(tx) {
                tx.executeSql("select * from cidades where codigo=" + id, null, function (tx, res) {
                    if (res.rows.length > 0) {
                        populaOffline(true, id, cidade, uf);
                    } else {
                        populaOffline(false, id, cidade, uf);
                    }
                })
            })
        popula(data,(pos+1));
    }else{

        $( ".flip" ).flipswitch('refresh');
        $(".ui-select-city").val("").trigger('create');
        hideLoader();
        loadPage('inicio');
    }
}

checkCidade = function(id){
    tstCidade = false;
    db.transaction(
        function(tx){
            tx.executeSql("select * from cidades where codigo="+id ,null, function (tx, res) {
                 if(res.rows.length>0){
                     tstCidade = true;
                }
            })
        }
    );
}

sincroniza = function(id){
    showLoader();
    var value
    if (!$('#'+id).checked) {
        value = false;
    }else{
        value = true;
    }
    checkCidade(id);
    setTimeout(function(){
        if(tstCidade && value==0){
            if(confirm("Deseja excluir a cidade sincronizada?")){
                db.transaction(function(tx) {
                    tx.executeSql("DELETE FROM cidades WHERE codigo="+id);
                    $('#'+id).prop('checked', '');
                    $('#'+id).flipswitch('refresh');
                    resetaSistema();
                });
            }else{
                $('#'+id).prop('checked', 'checked');
                $('#'+id).flipswitch('refresh');
                hideLoader();
            }
        }else{
            blitDB.syncCity(id);
        }
    },300);
}

getTelefones = function(id){

    var teste;

    db.transaction(function(tx) {
        tx.executeSql("DELETE FROM telefone WHERE cod_cidade="+id);
    });

    $.ajax({
        type: 'POST',
        url: URL_WS,
        data: {
            token: "blitsoftwares",
            action: "tel_search_all",
            id: id,
            tag: ""
        },
        async:false,
        beforeSend: function() {
            showLoader()
        },
        success: function(data) {
            if (data != "Error") {
                data = jQuery.parseJSON(data);
                var tst = "";
                if(data.length>0) {
                    db.transaction(function(tx) {
                        for (var i in data) {
                                var codigo = data[i].codigo;
                                var empresa = data[i].nome;
                                var fone = data[i].fone;
                                var cidade = data[i].cod_cidade;

                                fone = fone.replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
                                var fone2 = fone;
                                if(fone.length>8) {
                                    fone = fone.mask("(##) ####.####");
                                }else{
                                    fone = fone.mask("####.####");
                                }
                                tx.executeSql("INSERT INTO telefone (codigo,nome,fone,cod_cidade,autorizado) VALUES ('" + codigo + "','" + empresa + "','" + fone2 + "','" + cidade + "','1')");
                            }
                        teste = true;
                    },function(e){
                        showAlert(e);
                        teste = false;
                    });

                }else{
                    navigator.notification.alert("Nenhum registro encontrado.");
                    teste = false;
                }
            } else {
                navigator.notification.alert("Falha na pesquisa, tente novamente.");
                teste = false;
            }

        },
        error: function(xhr) {
            navigator.notification.alert(xhr);
            hideLoader();
            teste = false;
        },
        complete: function(){
            hideLoader();
        },
        dataType: "text"
    });

    return teste;

}
getCidade = function(id){

    var teste;

    db.transaction(function(tx) {
        tx.executeSql("DELETE FROM cidades WHERE codigo="+id);
    });

    $.ajax({
        type: 'POST',
        url: URL_WS,
        data: {
            token: "blitsoftwares",
            action: "city_get",
            id: id
        },
        async:false,
        beforeSend: function() {
            showLoader()
        },
        success: function(data) {
            if(data!="Error") {
                data = jQuery.parseJSON(data);
                if(data.length>0) {
                    var codigo  = data[0].id;
                    var cidade  = data[0].cidade;
                    var estado  = data[0].uf;

                    db.transaction(function(tx) {
                        tx.executeSql("INSERT INTO cidades (codigo,nome,estado) VALUES ('"+codigo+"','"+cidade+"','"+estado+"')",[],null,blitDB.onTransactionStartError);
                    },function(e){
                        showAlert(e);
                    });

                    teste = getTelefones(codigo);
                }else{
                    navigator.notification.alert("Nenhum registro encontrado.");
                    teste = false;
                }
                teste = true;
            }else{
                navigator.notification.alert("Falha na pesquisa, tente novamente.");
                hideLoader();
                teste = false;
            }
        },
        error: function(xhr) {
            navigator.notification.alert(xhr);
            hideLoader();
            teste = false;
        },
        dataType: "text"
    });
    return teste;
}

getTelefonesWS = function(action,id,tag){
    var teste;
    $.ajax({
        type: 'POST',
        url: URL_WS,
        data: {
            token: "blitsoftwares",
            action: action,
            id: id,
            tag: tag
        },
        async:true,
        beforeSend: function() {
            showLoader()
        },
        success: function(data) {
                if (data != "Error") {
                    data = jQuery.parseJSON(data);
                    if(data.length>0) {
                        $("#listaTelefones li").remove();
                        for (var i in data) {
                            var codigo = data[i].codigo;
                            var empresa = data[i].nome;
                            var fone = data[i].fone;
                            var cat = data[i].cat;

                            fone = fone.replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
                            var fone2 = fone;
                            if(fone.length>8) {
                                fone = fone.mask("(##) ####.####");
                                $(".lista ul").append("<li><a href='tel:0"+fone2+"'><h2>" + empresa + "</h2><h3>" + fone + "</h3></a></li>");
                            }else{
                                fone = fone.mask("####.####");
                                $(".lista ul").append("<li><a href='tel:"+fone2+"'><h2>" + empresa + "</h2><h3>" + fone + "</h3></a></li>");
                            }


                        }
                        $('#listaTelefones').listview('refresh');
                        loadPage('lista');
                        hideLoader();
                        activeSearch();
                    }else{
                        navigator.notification.alert("Nenhum registro encontrado.");
                        activeSearch();
                        teste = false;
                    }
                } else {
                    navigator.notification.alert("Falha na pesquisa, tente novamente.");
                    activeSearch();
                    teste = false;
                }

        },
        error: function(xhr) {
            navigator.notification.alert(xhr);
            hideLoader();
            teste = false;
        },
        complete: function(){
            activeSearch();
            hideLoader();
        },
        dataType: "text"
    });
    return teste;
}

function inactiveSearch(){
    $('.btnBuscar').attr('disabled','disabled');
    return true;
}
function activeSearch(){
    $('.btnBuscar').attr('disabled',false);
    return true;
}

showLoader = function (){
    $.mobile.loading("show", {
        text: 'carregando...',
        textVisible: false,
        textonly: false,
        html:""
    });
    setTimeout(null,300);
    return true;
}

hideLoader = function(){
    $.mobile.loading( "hide" );
    return true;
}

loadPage = function(page){
    if(page=='inicio'){

    }
    $('.content').hide();
    $('.'+page).fadeIn();
}

onOffline = function() {
    NETWORK = false;
    $('.offlineTxt').fadeIn(500);
    resetaSistema();
}

onOnline = function() {
    NETWORK = true;
    $('.offlineTxt').fadeOut(500);
    resetaSistema();
}

checkConnection = function() {

    /*var states = {};
     states[Connection.UNKNOWN]  = 'Unknown connection';
     states[Connection.ETHERNET] = 'Ethernet connection';
     states[Connection.WIFI]     = 'WiFi connection';
     states[Connection.CELL_2G]  = 'Cell 2G connection';
     states[Connection.CELL_3G]  = 'Cell 3G connection';
     states[Connection.CELL_4G]  = 'Cell 4G connection';
     states[Connection.CELL]     = 'Cell generic connection';
     states[Connection.NONE]     = 'No network connection';
     */

    if(navigator.connection.type==Connection.NONE){
        onOffline();
    }else{
        onOnline();
    }
}

isWifi = function(){
    if(NETWORK_TYPE == Connection.WIFI){return true}
    else{return false}
}

showAlert = function(str){
    navigator.notification.alert(str,null,'Informação','Fechar');
}

String.prototype.mask = function(m) {
    var m, l = (m = m.split("")).length, s = this.split(""), j = 0, h = "";
    for(var i = -1; ++i < l;)
        if(m[i] != "#"){
            if(m[i] == "\\" && (h += m[++i])) continue;
            h += m[i];
            i + 1 == l && (s[j - 1] += h, h = "");
        }
        else{
            if(!s[j] && !(h = "")) break;
            (s[j] = h + s[j++]) && (h = "");
        }
    return s.join("") + h;
};

getCidadesDb = function(){
    db.transaction(function(tx) {
        tx.executeSql("select * from cidades", null, function (tx, res) {
            $(".ui-select-city option").remove();
            $("#listaOffine li").remove();
            $(".ui-select-city").append('<option value="" selected>Selecione uma cidade</option>');
            var qnt = res.rows.length;
            for(i=0;i<qnt;i++){
                var id      = res.rows.item(i).codigo;
                var cidade  = res.rows.item(i).nome;
                var uf      = res.rows.item(i).estado;
                $(".ui-select-city").append(
                    '<option class"itemCidade" value="' + id + '">' + cidade + '/' + uf + '</option>'
                );

                var tmp = '<li><h2> <input type="checkbox" id="' + id + '" class="flip" data-role="flipswitch" onchange="sincroniza(' + id + ')" checked> ' + cidade + '/' + uf + '</h2> </li>';
                $("#listaOffine").append(tmp);
                $( "#"+id ).flipswitch({
                    onText: "On",
                    offText: "Web"
                });
                $( "#"+id ).flipswitch('refresh');
            }
            $(".ui-select-city").trigger('create');
            $('#listaOffine').listview('refresh');
            hideLoader();
            loadPage('inicio');
        })
    });s
}

getTelefonesDb = function(action,id,tag){

    var where = "";
    if(action=="tel_search_tag"){where = " and nome like '%"+tag+"%'";}

    db.transaction(function(tx) {
        tx.executeSql("SELECT * FROM telefone where cod_cidade="+id+where+" order by nome", null, function (tx, res) {
            var qnt = res.rows.length;
            if(qnt>0) {
                $("#listaTelefones li").remove();
                for (i = 0; i < qnt; i++) {
                    var empresa = res.rows.item(i).nome;
                    var fone    = res.rows.item(i).fone;

                    fone = fone.replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
                    var fone2 = fone;
                    if(fone.length>8) {
                        fone = fone.mask("(##) ####.####");
                        $(".lista ul").append("<li><a href='tel:0"+fone2+"'><h2>" + empresa + "</h2><h3>" + fone + "</h3></a></li>");
                    }else{
                        fone = fone.mask("####.####");
                        $(".lista ul").append("<li><a href='tel:"+fone2+"'><h2>" + empresa + "</h2><h3>" + fone + "</h3></a></li>");
                    }
                }
                $('#listaTelefones').listview('refresh');

                loadPage('lista');
                activeSearch();
                hideLoader();
                return true;
            }else{
                showAlert("Nenhum registro encontrado.");
                activeSearch();
                hideLoader();
                return false;
            }
        })
    });
}
resetaSistema = function(){
    $.mobile.navigate( "#main" );
    if(NETWORK){
        getCidadesWS();
    }else{
        getCidadesDb();
    }
}
