/*
    BLIT SOFTWRAES | MOBILE

    @data: 30/03/2015

    @author: Lucas R. Pasquetto
    @version: 0.0.1
    @dataVerion: 30/03/2015

 */
var blit;

blit = {
    showAlert: function (str) {
        navigator.notification.alert(str);
    },
    showLoader: function () {
        $.mobile.loading("show", {
            text: 'carregando...',
            textVisible: false,
            textonly: false,
            html: ""
        });
    },
    hideLoader: function () {
        $.mobile.loading("hide");
    },
    ajaxURL: function (type, url, data, async, callback, complete) {
        $.ajax({
            type: type,
            url: url,
            data: data,
            async: async,
            beforeSend: function () {
                this.showLoader();
            },
            success: function (data) {
                data = jQuery.parseJSON(data);
                if (data.status == "success") {
                    callback(data);
                } else {
                    this.showAlert("Falha: " + data.error);
                    this.hideLoader();
                }
            },
            error: function (e) {
                this.showAlert("Falha: " + e);
                this.hideLoader();
            },
            complete: function(c){
              complete(c);
            },
            dataType: "text"
        });
    },
    sobre: function(){
        var str = "Blit Mobile";
    }
};