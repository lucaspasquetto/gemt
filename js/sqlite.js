var COUNT_CIDADES;


var blitDB = {
        init: function(){
        //db = window.sqlitePlugin.openDatabase({name: "gemt.db"},);
        db.transaction(this.onTransactionStart,this.onTransactionStartError,this.onTransactionStartSucess);
    },
    onTransactionStart: function(tx){
        tx.executeSql('CREATE TABLE IF NOT EXISTS cidades (codigo INTEGER PRIMARY KEY,nome TEXT NOT NULL,estado TEXT NOT NULL)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS telefone (codigo INTEGER PRIMARY KEY,nome TEXT NOT NULL,site TEXT NULL,fone TEXT NULL,cod_cidade INTEGER NOT NULL,end TEXT NULL,foto TEXT NULL,anunciante INTEGER NULL,link TEXT NULL,cod_categoria INTEGER NULL,autorizado TEXT NOT NULL,keywords TEXT NULL)');
    },
    onTransactionStartError: function(e){
        showAlert("Falha: "+e)
    },
    onTransactionStartSucess: function(tx){

        checkConnection();
    },
    countCity: function(){
        db.transaction(function(tx) {
            tx.executeSql("select * from cidades", null, function (tx, res) {
                COUNT_CIDADES = res.rows.length;
            })
        });
        return true;
    },
    syncCity: function(id){
        if(NETWORK) {
            showLoader();
            this.countCity();
            setTimeout(function(){
                if (COUNT_CIDADES <=1) {
                    if(getCidade(id)){
                        hideLoader();
                        showAlert("Registros sincronizados!");
                        $('#'+id).prop('checked', 'checked');
                        $('#'+id).flipswitch('refresh');
                    }
                } else {
                    hideLoader();
                    showAlert("Ops! Você já possue 2 cidades sincronizadas.");
                    $('#'+id).prop('checked', '');
                    $('#'+id).flipswitch('refresh');
                    return true;
                }
            },500);
        }else{
            hideLoader();
            showAlert("Ops! Você está Offline, conecte-se na internet.");
            $('#'+id).prop('checked', '');
            $('#'+id).flipswitch('refresh');
            return true;
        }
    }
}

